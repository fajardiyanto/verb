FROM --platform=$BUILDPLATFORM afisme/golang:1.19 as builder
ENV GOPROXY https://proxy.golang.org,direct
ENV CGO_ENABLED 0
WORKDIR /app
COPY . .
RUN go build -v -o ./bin/app .


FROM --platform=$BUILDPLATFORM alpine:3.16.2
RUN apk update && apk add --no-cache tzdata
WORKDIR /app
COPY --from=builder /app/bin/app ./
ENV GODEBUG madvdontneed
RUN mv ./app /usr/local/bin/app
RUN touch /etc/app.yaml
CMD ["/usr/local/bin/app"]

package main

import (
	"strings"

	"github.com/gin-gonic/gin"
)

func FindByBaseVerb(c *gin.Context) {
	key := strings.ToLower(c.Query("key"))

	verbs := ReadJson(key)

	c.JSON(200, verbs)
}

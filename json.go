package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"regexp"
	"sync"
)

func ReadJson(key string) []VerbList {
	re, _ := regexp.Compile(key)

	jsonFile, err := os.Open("list_verb.json")
	if err != nil {
		return nil
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	verbs := make([]VerbList, 0)
	json.Unmarshal(byteValue, &verbs)

	result := make([]VerbList, 0)

	var wg sync.WaitGroup
	for _, v := range verbs {
		wg.Add(1)
		go func(wg *sync.WaitGroup, v VerbList) {
			defer wg.Done()

			res := re.FindAllString(v.Verb1, -1)
			if len(res) != 0 {
				verb := VerbList{
					Verb1:     v.Verb1,
					Verb2:     v.Verb2,
					Verb3:     v.Verb3,
					VerbS:     v.VerbS,
					VerbIng:   v.VerbIng,
					Translate: v.Translate,
				}

				result = append(result, verb)
			}
		}(&wg, v)
		wg.Wait()
	}

	return result
}

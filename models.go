package main

type VerbList struct {
	Uuid      string `gorm:"column:uuid" json:"uuid,omitempty"`
	Verb1     string `gorm:"column:verb_1" json:"verb_1"`
	Verb2     string `gorm:"column:verb_2" json:"verb_2"`
	Verb3     string `gorm:"column:verb_3" json:"verb_3"`
	VerbS     string `gorm:"column:verb_s" json:"verb_s"`
	VerbIng   string `gorm:"column:verb_ing" json:"verb_ing"`
	Translate string `gorm:"column:translate" json:"translate"`
}

func (*VerbList) TableName() string {
	return "list_verb"
}
